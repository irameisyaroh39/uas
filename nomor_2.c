/*
  NAMA    :Ira Meisyaroh
  NIM     :302230005
  KELAS   :SI 1 Pagi
  */



#include <stdio.h>

char hitungHurufMutu(double nilai);

void tampilkanKomponen(double absen, double uts, double tugas, double uas, double quiz);

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    printf("Masukkan nilai Absen: ");
    scanf("%lf", &absen);
    printf("Masukkan nilai UTS: ");
    scanf("%lf", &uts);
    printf("Masukkan nilai Tugas: ");
    scanf("%lf", &tugas);
    printf("Masukkan nilai UAS: ");
    scanf("%lf", &uas);
    printf("Masukkan nilai Quiz: ");
    scanf("%lf", &quiz);

    tampilkanKomponen(absen, uts, tugas, uas, quiz);

    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;

    Huruf_Mutu = hitungHurufMutu(nilai);

    printf("Huruf Mutu : %c\n", Huruf_Mutu);

    return 0;
}

char hitungHurufMutu(double nilai) {
    char Huruf_Mutu;
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';
    return Huruf_Mutu;
}

void tampilkanKomponen(double absen, double uts, double tugas, double uas, double quiz) {
    printf("Absen = %.2f UTS = %.2f\n", absen, uts);
    printf("Tugas = %.2f UAS = %.2f\n", tugas, uas);
    printf("Quiz = %.2f\n", quiz);
}
